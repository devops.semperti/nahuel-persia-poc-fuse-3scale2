package com.semperti.hipotecario.poc.fuse.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TelefonosIntegrador implements Serializable{
	protected List<TelefonoIntegrador> telefonos = new ArrayList<TelefonoIntegrador>();
	public TelefonosIntegrador() {}

	public List<TelefonoIntegrador> getTelefonos() {
		return telefonos;
	}

	public void setTelefonos(List<TelefonoIntegrador> telefonos) {
		this.telefonos = telefonos;
	}
}
