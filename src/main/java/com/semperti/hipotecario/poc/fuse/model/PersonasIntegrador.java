package com.semperti.hipotecario.poc.fuse.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PersonasIntegrador implements Serializable {
	List<PersonaIntegrador> personas = new ArrayList<PersonaIntegrador>();
	public PersonasIntegrador() {}
	
	public PersonasIntegrador(List<PersonaIntegrador> personas) {
		this.personas= personas;
	}
	public List<PersonaIntegrador> getPersonas() {
		return personas;
	}

	public void setPersonas(List<PersonaIntegrador> personas) {
		this.personas = personas;
	}
}
