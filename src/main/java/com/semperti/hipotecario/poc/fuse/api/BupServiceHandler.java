package com.semperti.hipotecario.poc.fuse.api;

import org.apache.camel.Exchange;

import org.apache.camel.component.cxf.common.message.CxfConstants;

import com.semperti.hipotecario.poc.fuse.model.Domicilio;
import com.semperti.hipotecario.poc.fuse.model.Domicilios;
import com.semperti.hipotecario.poc.fuse.model.PersonaIntegrador;
import com.semperti.hipotecario.poc.fuse.model.PersonasIntegrador;
import com.semperti.hipotecario.poc.fuse.model.TelefonoIntegrador;
import com.semperti.hipotecario.poc.fuse.model.TelefonosIntegrador;

public class BupServiceHandler {
	public void obtenerDomicilios(Exchange exchange) {
		exchange.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_USING_HTTP_API, Boolean.TRUE);
		exchange.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_RESPONSE_CLASS, Domicilios.class);
	}

	public void obtenerDomicilio(Exchange exchange) {
		exchange.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_USING_HTTP_API, Boolean.TRUE);
		exchange.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_RESPONSE_CLASS, Domicilio.class);
	}

	public void obtenerPersonas(Exchange exchange) {
		exchange.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_USING_HTTP_API, Boolean.TRUE);
		exchange.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_RESPONSE_CLASS, PersonasIntegrador.class);
	}

	public void obtenerDomiciliosDePersona(Exchange exchange) {
		exchange.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_USING_HTTP_API, Boolean.TRUE);
		exchange.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_RESPONSE_CLASS, Domicilios.class);
	}

	public void obtenerPersona(Exchange exchange) {
		exchange.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_USING_HTTP_API, Boolean.TRUE);
		exchange.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_RESPONSE_CLASS, PersonaIntegrador.class);
	}

	public void obtenerTelefonosDePersona(Exchange exchange) {
		exchange.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_USING_HTTP_API, Boolean.TRUE);
		exchange.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_RESPONSE_CLASS, TelefonosIntegrador.class);
	}

	public void obtenerTelefonos(Exchange exchange) {
		exchange.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_USING_HTTP_API, Boolean.TRUE);
		exchange.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_RESPONSE_CLASS, TelefonosIntegrador.class);
	}

	public void obtenerTelefono(Exchange exchange) {
		exchange.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_USING_HTTP_API, Boolean.TRUE);
		exchange.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_RESPONSE_CLASS, TelefonoIntegrador.class);
	}
}
